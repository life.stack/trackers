import express from 'express'
import 'express-async-errors'
import { json } from 'body-parser'
import cookieSession from 'cookie-session'
import {
  currentUser,
  errorHandler,
  livenessRouter,
  NotFoundError,
} from '@life.stack/common'
import { newEventRouter } from './routes/trackers/newEvent'
import { showEventsRouter } from './routes/trackers/showEvents'
import { newCategoryRouter } from './routes/trackers/newCategory'
import { showCategoryRouter } from './routes/trackers/showCategory'
import { showCategoriesRouter } from './routes/trackers/showCategories'

const app = express()
app.set('trust proxy', true) // App is behind nginx proxy and should trust it
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
)
app.use(currentUser)

//  App Specific
// /api/trackers
app.use(newCategoryRouter)
app.use(newEventRouter)
app.use(showEventsRouter)
app.use(showCategoriesRouter)
app.use(showCategoryRouter)

// Extra
app.use(livenessRouter)

// If app routes don't work, throw a not found
app.all('*', () => {
  throw new NotFoundError()
})

// Error Handler
app.use(errorHandler)

export { app }
