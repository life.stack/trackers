import request from 'supertest'
import { app } from '../../../app'
import { Tracker } from '../../../models/tracker'
import { validCategory } from './newCategory.test'

it('has a route handler on /api/trackers/:categoryId for POST requests', async () => {
  const user = global.signin()
  const { body: createdCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)
    .expect(201)

  const response = await request(app)
    .post(`/api/trackers/${createdCategory.id}`)
    .set('Cookie', user)
    .send({})

  expect(response.status).not.toEqual(404)
})

it('POST /api/trackers/:categoryId requires auth', async () => {
  await request(app).post('/api/trackers').send({ value: 42 }).expect(401)

  const user = global.signin()
  const { body: createdCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)
    .expect(201)

  const timestamp = new Date().toISOString()
  console.log(timestamp)

  const response = await request(app)
    .post(`/api/trackers/${createdCategory.id}`)
    .set('Cookie', user)
    .send({ value: 42, timestamp: timestamp })
    .expect(201)

  console.log(response.statusCode)
  console.log(response.body)
})

it('creates a new event', async () => {
  const user = global.signin()

  const { body: createdCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)
    .expect(201)

  const { body: event1 } = await request(app)
    .post(`/api/trackers/${createdCategory.id}`)
    .set('Cookie', user)
    .send({ value: 42, timestamp: new Date().toISOString() })
    .expect(201)

  const event1InDb = await Tracker.findById(event1.id)

  expect(event1.id).toEqual(event1InDb!.id)
  expect(event1.value).toEqual(event1InDb!.value)
  expect(new Date(event1.timestamp)).toEqual(event1InDb!.timestamp)
})
