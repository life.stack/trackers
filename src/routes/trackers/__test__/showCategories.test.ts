import request from 'supertest'
import { app } from '../../../app'
import { validCategory } from './newCategory.test'

const setup = async () => {
  const user = global.signin()

  const { body: testCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)

  const { body: testEvent } = await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 10, timestamp: new Date() })

  return { user, testCategory, testEvent }
}

it('has a route handler GET /api/trackers', async () => {
  const { user } = await setup()

  await request(app).get(`/api/trackers`).set('Cookie', user).send().expect(200)
})

it('requires Auth', async () => {
  await request(app).get(`/api/trackers`).send().expect(401)
})

it('retrieves all tracker categories for a group', async () => {
  const { testCategory, user } = await setup()

  await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)

  await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)

  const { body: getCategories } = await request(app)
    .get(`/api/trackers`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(getCategories.length).toEqual(3)
})
