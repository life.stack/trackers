import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'
import { validCategory } from './newCategory.test'

const setup = async () => {
  const user = global.signin()

  const { body: testCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)

  const { body: testEvent } = await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 10, timestamp: new Date() })

  return { user, testCategory, testEvent }
}

it('has a route handler GET /api/trackers/:categoryId/events', async () => {
  const { testCategory, user } = await setup()

  await request(app)
    .get(`/api/trackers/${testCategory.id}/events`)
    .set('Cookie', user)
    .send()
    .expect(200)
})

it('requires Auth', async () => {
  await request(app)
    .get(`/api/trackers/${new mongoose.Types.ObjectId()}/events`)
    .send()
    .expect(401)
})

it('retrieves all events from a category', async () => {
  const { testCategory, user } = await setup()

  await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 11, timestamp: new Date() })
    .expect(201)

  await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 12, timestamp: new Date() })
    .expect(201)

  await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 13, timestamp: new Date() })
    .expect(201)

  await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 14, timestamp: new Date() })
    .expect(201)

  const { body: getEvents } = await request(app)
    .get(`/api/trackers/${testCategory.id}/events`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(getEvents.length).toEqual(5)
})
