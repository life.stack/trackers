import request from 'supertest'
import { app } from '../../../app'

export const validCategory = {
  name: "user's weight",
  type: 'weight',
  unit: 'lbs',
}

it('has a route handler on /api/trackers for POST requests', async () => {
  const response = await request(app).post('/api/trackers').send({})

  expect(response.status).not.toEqual(404)
})

it('POST /api/trackers requires auth', async () => {
  await request(app).post('/api/trackers').send(validCategory).expect(401)

  await request(app)
    .post('/api/trackers')
    .set('Cookie', global.signin())
    .send(validCategory)
    .expect(201)
})

it('creates a new tracker category', async () => {
  const { body: createdCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', global.signin())
    .send(validCategory)
    .expect(201)

  expect(createdCategory.name).toEqual(validCategory.name)
  expect(createdCategory.type).toEqual(validCategory.type)
  expect(createdCategory.unit).toEqual(validCategory.unit)
  expect(createdCategory.id).toBeDefined()
})
