import request from 'supertest'
import { app } from '../../../app'
import { Category } from '../../../models/category'
import { validCategory } from './newCategory.test'

const setup = async () => {
  const user = global.signin()

  const { body: testCategory } = await request(app)
    .post('/api/trackers')
    .set('Cookie', user)
    .send(validCategory)

  const { body: testEvent } = await request(app)
    .post(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send({ value: 10, timestamp: new Date() })

  return { user, testCategory, testEvent }
}

it('has a route handler GET /api/trackers/:trackerId', async () => {
  const { user, testCategory } = await setup()

  await request(app)
    .get(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)
})

it('requires Auth', async () => {
  const { user, testCategory } = await setup()

  await request(app).get(`/api/trackers/${testCategory.id}`).send().expect(401)
  await request(app)
    .get(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)
})

it('retrieves a tracker given an id', async () => {
  const { testCategory, user } = await setup()

  const { body: getCategory } = await request(app)
    .get(`/api/trackers/${testCategory.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  const categoryInDb = await Category.findById(testCategory.id)

  expect(getCategory.id).toEqual(testCategory.id)
  expect(getCategory.id).toEqual(categoryInDb!.id)
  expect(getCategory.name).toEqual(categoryInDb!.name)
  expect(getCategory.type).toEqual(categoryInDb!.type)
  expect(getCategory.unit).toEqual(categoryInDb!.unit)
})
