import express, { Request, Response } from 'express'
import { requireAuth } from '@life.stack/common'
import { Category } from '../../models/category'

const showCategoriesRouter = express.Router()

showCategoriesRouter.get(
  '/api/trackers',
  requireAuth,
  async (req: Request, res: Response) => {
    let categories = await Category.find({
      groupId: req.currentUser!.groups[0],
    })

    if (!categories || categories.length === 0) {
      categories = []
    }

    res.status(200).send(categories)
  }
)

export { showCategoriesRouter }
