import express, { Request, Response } from 'express'
import { Category } from '../../models/category'
import { body } from 'express-validator'
import { requireAuth, validateRequest } from '@life.stack/common'

const newCategoryRouter = express.Router()

const validTypes = ['weight', 'temperature', 'custom']

newCategoryRouter.post(
  '/api/trackers',
  requireAuth,
  [
    body('name')
      .not()
      .isEmpty()
      .isLength({ min: 1, max: 256 })
      .withMessage('name must be provided'),
    body('type')
      .isIn(validTypes)
      .withMessage(`type must be one of: ${validTypes.toString()}`),
    body('unit')
      .not()
      .isEmpty()
      .isLength({ min: 1, max: 128 })
      .withMessage('unit must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name, type, unit } = req.body

    const category = Category.build({
      name,
      type,
      unit,
      groupId: req.currentUser!.groups[0],
      ownerId: req.currentUser!.id!,
    })
    await category.save()

    res.status(201).send(category)
  }
)

export { newCategoryRouter }
