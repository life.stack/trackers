import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Category } from '../../models/category'
import { Tracker } from '../../models/tracker'

const showEventsRouter = express.Router()

showEventsRouter.get(
  '/api/trackers/:categoryId/events',
  requireAuth,
  async (req: Request, res: Response) => {
    const { categoryId } = req.params

    const category = await Category.findByGroup(categoryId, req.currentUser!)

    if (!category) {
      throw new NotFoundError()
    }

    const events = await Tracker.findByCategory(categoryId)

    res.status(200).send(events)
  }
)

export { showEventsRouter }
