import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth, validateRequest } from '@life.stack/common'
import { Category } from '../../models/category'
import { Tracker } from '../../models/tracker'
import { body } from 'express-validator'

const newEventRouter = express.Router()

newEventRouter.post(
  '/api/trackers/:categoryId',
  requireAuth,
  [body('value').isNumeric(), body('timestamp').isISO8601()],
  validateRequest,
  async (req: Request, res: Response) => {
    const { categoryId } = req.params
    const { value, timestamp } = req.body

    const category = await Category.findByGroup(categoryId, req.currentUser!)
    if (!category) {
      throw new NotFoundError()
    }

    const event = Tracker.build({
      value,
      categoryId,
      timestamp,
    })
    await event.save()

    res.status(201).send(event)
  }
)

export { newEventRouter }
