import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Category } from '../../models/category'

const showCategoryRouter = express.Router()

showCategoryRouter.get(
  '/api/trackers/:categoryId',
  requireAuth,
  async (req: Request, res: Response) => {
    const { categoryId } = req.params

    const category = await Category.findByGroup(categoryId, req.currentUser!)

    if (!category) {
      throw new NotFoundError()
    }

    res.status(200).send(category)
  }
)

export { showCategoryRouter }
