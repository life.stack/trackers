import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

// An interface that describes the properties that are required to create a new Tracker
interface CategoryAttrs {
  name: string
  type: string
  unit: string
  groupId: string
  ownerId: string
}

// an interface that describes the properties a Tracker document has
export interface CategoryDoc extends mongoose.Document {
  name: string
  type: string
  unit: string
  groupId: string
  ownerId: string
  version: number
}

// An interface that describes the properties that a Tracker model has
interface CategoryModel extends mongoose.Model<CategoryDoc> {
  build(attrs: CategoryAttrs): CategoryDoc
  findByGroup(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<CategoryDoc | null>
}

const categorySchema = new mongoose.Schema(
  {
    groupId: {
      type: String,
      required: true,
    },
    ownerId: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    unit: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

categorySchema.set('versionKey', 'version')
categorySchema.plugin(updateIfCurrentPlugin)

categorySchema.statics.findByGroup = (id, currentUser) => {
  return Category.findOne({
    _id: id,
    groupId: currentUser.groups[0],
  })
}
categorySchema.statics.build = (attrs: CategoryAttrs) => {
  return new Category(attrs)
}

const Category = mongoose.model<CategoryDoc, CategoryModel>(
  'Category',
  categorySchema
)

export { Category }
