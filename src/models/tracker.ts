import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

// An interface that describes the properties that are required to create a new Tracker
interface TrackerAttrs {
  value: number
  categoryId: string
  timestamp: Date
}

// an interface that describes the properties a Tracker document has
interface TrackerDoc extends mongoose.Document {
  value: number
  timestamp: Date
  categoryId: string
  version: number
}

// An interface that describes the properties that a Tracker model has
interface TrackerModel extends mongoose.Model<TrackerDoc> {
  build(attrs: TrackerAttrs): TrackerDoc
  findByCategory(categoryId: string): Promise<TrackerDoc | null>
}

const trackerSchema = new mongoose.Schema(
  {
    value: {
      type: Number,
      required: true,
    },
    timestamp: { type: Date, required: true },
    categoryId: {
      type: mongoose.Types.ObjectId,
      ref: 'Category',
      required: true,
    },
  },
  {
    autoCreate: false,
    autoIndex: false,
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

trackerSchema.set('versionKey', 'version')
trackerSchema.plugin(updateIfCurrentPlugin)

trackerSchema.statics.findByCategory = (categoryId) => {
  return Tracker.find({
    categoryId: categoryId,
  })
}
trackerSchema.statics.build = (attrs: TrackerAttrs) => {
  return new Tracker({
    value: attrs.value,
    categoryId: attrs.categoryId,
    timestamp: attrs.timestamp,
  })
}

const Tracker = mongoose.model<TrackerDoc, TrackerModel>(
  'Tracker',
  trackerSchema
)

// Mongoose cannot create the time series collection for me yet
// https://github.com/Automattic/mongoose/issues/10611#issuecomment-905926363
Tracker.createCollection({
  timeseries: {
    timeField: 'timestamp',
    metaField: 'categoryId',
    granularity: 'hours',
  },
})
  .then((value) => console.log('Tracker collection created', value))
  .catch((err) => {
    console.error('Failed to create Tracker collection', err)
  })

export { Tracker }
